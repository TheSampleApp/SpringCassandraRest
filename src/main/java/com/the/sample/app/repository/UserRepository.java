package com.the.sample.app.repository;

import com.the.sample.app.model.User;
import org.springframework.data.cassandra.repository.ReactiveCassandraRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface UserRepository extends ReactiveCassandraRepository<User, String> {
    Mono<User> findByEmail(String email);
}
